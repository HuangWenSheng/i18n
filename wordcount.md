# Word Count

## English

Stat | Value
---- | -----
total files | 206
total words | 214894
unique words | 9762
average words per file | 1043

## All Languages

Stat | Value
---- | -----
total files | 5477
total words | 4196399
unique words | 89723
average words per file | 766
